                        
org 100h
endstr db "Ganaste!",10,13
       db "Tu puntuacion es: 75.",'$'
           
board   db "****************************",10,13
        db "*** X * X * X * X * X * X **",10,13
        db "****************************",10,13
        db "*** X * X * X * X * X * X **",10,13
        db "****************************",10,13
        db "*** X * X * X * X * X * X **",10,13
        db "****************************",10,13
        db "*** X * X * X * X * X * X **",10,13
        db "****************************",10,13
        db "*** X * X * X * X * X * X **",10,13
        db "****************************",'$' 
               
up EQU 'y'
down EQU 'n'
left EQU 'g'
right EQU 'j'
uncover EQU 'h'
pos1 dw ?
pos2 dw ?
step_x EQU 4            ; Separacion horizontal de las posiciones licitas del tablero
step_y EQU 2            ; Separacion vertical de las posiciones licitas del tablero
DJ db 0                 ; Cantidad de posiciones descubiertas en la jugada actual
move1_row db '0'        ; variables para guardar la posicion de
move1_column db '0'     ; las movidas en de la jugada en curso
move2_row db '0'        ;
move2_column db '0'     ;
val_mov1 db 0           ; variables para almacenar los valores descubiertos
val_mov2 db 0           ; en la jugada en curso
puntos db 0             ; Contador del puntaje del jugador

txt db 10,13,"Puntaje:",'$'
transform1 db -1
transform2 db 0
aux db 0

couples db "AHEKBO"
        db "NCJGID"
        db "GMAEKN"
        db "FMHLCD"
        db "BJILFO"
                             
inicio:
            MOV move1_row, 1
            MOV move1_column,4
            MOV BX, 0
            MOV DX, 0
            
imprimirTablero:   
                   MOV DX, offset board
                   ADD DX,BX
                   mov Ax, 0
                   mov Ah, 09h
                   INT 21h
                   MOV Ax, 0
                   JMP mostrarPuntaje
                   JMP auxCursor
SetCursor proc              
        mov ah, 02h
        mov bh, 00
        int 10h
        ret
SetCursor endp              
cleaneimp:
            mov ah, 0h
            int 10h
            CMP puntos, 75
            JE imprimirFinal
            JNE imprimirTablero          
esperar:    
            CMP DJ,2
            JE comparacion  ;aca adentro se ve si val1 y val2 son =
            MOV Ax, 0
            MOV Bx, 0
            INT 16h
            CMP Al, uncover
            JE davuelta
            JMP move

davuelta:
            CMP val_mov1,0
            JE  davuelta1
            JNE testeaIgualdad
testeaIgualdad: ;se fija que las posicion de move1
                ;sea distinta de move2
                ;solo para tratar de aumentar rendimiento    
            MOV Ax,0
            MOV Ah,move2_row
            CMP move1_row, Ah
            JE testeaIgualdadParte2
            JNE davuelta2 
testeaIgualdadParte2: 
        Mov Al,move2_column
        CMP move1_column,Al
        JE esperar
        JNE davuelta2
        
davuelta1:  ;Busca la letra en la matriz de letras
            ;y la pone en val_mov1
            MOV Ax, 0
            MOV Dx, 0
            MOV Bx,0
            MOV Al, 6
            MOV Dh, move1_row
            MUL Dh
            ;Al x Dh => Ax   Tengo en que fila estoy
            MOV Bx,Ax
            Mov Ax, 0
            MOV Dh,0
            MOV Dh,move1_column
            MOV Al,1
            MUL dh;Multiplicamos por 1 para pasar
                  ;lo que esta en Dh, a Ax, osea
                  ;de byte a word.
            ADD Bx,Ax
            MOV Ax,0
            MOV Ah, couples[Bx]
            MOV val_mov1, Ah
            JMP reemplazar1 
            
davuelta2:  ;Busca la letra en la matriz de letras
            ;y la pone en val_mov2   
            MOV Ax, 0
            MOV Dx, 0
            MOV Bx,0
            MOV Al, 6
            MOV Dh, move2_row
            MUL Dh    ;Al x Dh => Ax   Tengo en que fila estoy
            MOV Bx,Ax
            Mov Ax, 0
            MOV Dh,0
            MOV Dh,move2_column
            MOV Al,1 ;Multiplicamos por 1 para pasar
                     ;lo que esta en Dh, a Ax, osea
                     ;de byte a word.
            MUL dh 
            ADD Bx,Ax
            MOV Ax,0
            MOV Ah, couples[Bx]
            MOV val_mov2, Ah
            JMP reemplazar2 
reemplazar1: ;reemplaza la letra hallada en davuelta1
             ;en el tablero
            MOV Ax,0
            MOV Dx, 0
            MOV Bx,0
            MOV Al, move1_row
            MOV Dh, step_y
            MUL Dh
            MOV Dh,0
            MOV Dh,30
            MUL Dh          
            MOV Bx,Ax
            ADD Bx,30
            MOV Ax,0
            MOV Dx,0
            MOV Al, move1_column
            MOV Dh,step_x
            MUL Dh  ; 4*columna => Ax
            ADD Ax,Bx
            MOV Bx, Ax 
            MOV Dh,0
            ADD Bx,4
            MOV Dh,val_mov1
            CMP board[Bx], "X"
            JNE aux1
            MOV board[Bx], Dh
            MOV pos1,Bx
            MOV Bx,0
            MOV Dx,0
            JMP setMove2
aux1:       ; setea el val_mov1 puesto en davuelta1
            ; en 0 si no era una jugada "legal"
            MOV val_mov1,0
            JMP esperar  
setMove2:   ;setea el (x,y) de move1 en move2
            ;Aumenta el var DJ y salta a limpiarPant
            
            MOV Ax,0
            MOV Ah, move1_row
            MOV move2_row, Ah
            MOV Ax,0
            MOV Ah, move1_column
            MOV move2_column, Ah
            INC DJ                        
            JMP cleaneimp           
reemplazar2:;reemplaza la letra hallada en davuelta2
            ;en el tablero            
            MOV Ax,0
            MOV Dx, 0
            MOV Bx,0
            MOV Al, move2_row
            MOV Dh , step_y
            MUL Dh
            MOV Dh,0
            MOV Dh,30
            MUL Dh
            MOV Bx, Ax
            ADD Bx,30
            MOV Ax,0
            MOV Dx,0
            MOV Al, move2_column
            MOV Dh,step_x
            MUL Dh  ; 4*columna => Ax
            ADD Ax,Bx
            MOV Bx, Ax 
            MOV Dh,0
            ADD Bx,4
            MOV Dh,val_mov2
            CMP board[Bx], "X"
            JNE aux2
            MOV board[Bx], Dh
            MOV pos2,Bx
            MOV Bx,0
            MOV Dx,0
            MOV Ax,0
            INC DJ
            JMP cleaneimp                        
aux2:   ; setea el val_mov1 puesto en davuelta2
        ; en 0 si no era una jugada "
        MOV val_mov2,0
        jmp esperar           
comparacion:   ;aca adentro se ve si val1 y val2 son = 
          Mov Ax, 0
          Mov Ah, val_mov1
          MOV DJ,0
          CMP Ah, val_mov2
          JE hit
          JNE recover
hit:     ;Si son = las letras dadas vueltas en la jugada
         ;se aumentan los puntos
         ADD puntos,5
         JMP restaurar        
recover:;Si son !=
        ;Vuelve a poner las X en el tablero
        MOV Bx,0
        MOV Bx,pos1
        MOV board[Bx],"X"
        MOV Bx,0
        MOV Bx,pos2
        MOV board[Bx],"X"
        MOV Bx,0
        MOV Bh,puntos
        JMP restaurar
restaurar:
        ;vacia todo para poder hacer otra jugada
        MOV Bx,0
        MOV Ax,0
        MOV Dx,0
        MOV pos1,0
        MOV pos2,0
        MOV val_mov1,0
        MOV val_mov2,0
        MOV Ah, move2_row
        MOV move1_row, Ah
        MOV move2_row,-1
        MOV Ah,0
        MOV Ah, move2_column
        MOV move1_column, Ah
        MOV move2_column,-1
        CMP puntos, 75
        JE finalizar
        MOV Ah,0
        JMP cleaneimp
auxCursor:
            CMP val_mov1,0
            JE  auxCursor1
            JNE auxCursor2
auxCursor1: 
         MOV Dx,0
         MOV Ax,0
         MOV Dh,move1_row
         MOV Al,step_y
         MUL dh
         MOV Bx,0
         MOV Bh,1
         Div Bh
         MOV Dh,0
         MOV Dh,Al
         INC Dh
         MOV Ax,0
         MOV Dl,move1_column
         MOV Al,step_x
         MUL dl
         MOV Bh,0
         MOV Bh,1h
         Div Bh
         MOV Dl,Al
         ADD Dl,4
         call SetCursor
         MOV Dx,0
         JMP esperar

auxCursor2: 
         MOV Dx,0
         MOV Ax,0
         MOV Dh,move2_row
         MOV Al,step_y
         MUL dh
         MOV Bx,0
         MOV Bh,1
         Div Bh
         MOV Dh,0
         MOV Dh,Al
         INC Dh
         MOV Ax,0
         MOV Dl,move2_column
         MOV Al,step_x
         MUL dl
         MOV Bh,0
         MOV Bh,1h
         Div Bh
         MOV Dl,Al
         ADD Dl,4
         call SetCursor
         MOV Dx,0
         JMP esperar
                 
move:   ;revisa si se uso una de las teclas validas
        ;y se ejecuta de acorde a lo ingresado
        CMP Al,up
        JE mup
        CMP Al,down
        JE mdown
        CMP Al, left
        JE mleft
        CMP Al, right
        JE mright
        JMP esperar               
mup:
        CMP val_mov1,0
        JE  mup1
        JNE mup2   
mup1:
         CMP move1_row,0
         JE  auxCursor1
         DEC move1_row
         JMP auxCursor1
mup2:
         CMP move2_row,0
         JE  auxCursor2
         DEC move2_row
         JMP auxCursor2   
mdown:
        CMP val_mov1,0
        JE  mdown1
        JNE mdown2
mdown1:
         CMP move1_row,4
         JE  auxCursor1
         INC move1_row
         JMP auxCursor1                
mdown2:
         CMP move2_row,4
         JE  auxCursor2
         INC move2_row
         JMP auxCursor2
mleft:
        CMP val_mov1,0
        JE  mleft1
       JNE mleft2  
mleft1:
       CMP move1_column, 0
       JE  auxCursor1
       DEC move1_column
       JMP auxCursor1     
mleft2:
       CMP move2_column, 0
       JE  auxCursor2
       DEC move2_column
       JMP auxCursor2
mright:
        CMP val_mov1,0
        JE  mright1
        JNE mright2       
mright1:
       CMP move1_column, 5
       JE  auxCursor1
       INC move1_column
       JMP auxCursor1
mright2:
       CMP move2_column, 5
       JE  auxCursor2
       INC move2_column
       JMP auxCursor2
       
mostrarPuntaje:
       MOV Ax,0
       MOV Ah, puntos
       MOV aux,Ah
       MOV Dx,0
       MOV Ah, 09h
       MOV Dx,offset txt
       INT 21h      
dividir:
        Mov Ah,0
        MOV Dx,0
        MOV Dh,1
        MOV Dl,10
        Mov Al, aux
        Mul Dh ;convierto aux, que es un byte, en word
               ;multiplicando por 1
        Div Dl ;Divido por 10, para dividir al puntaje
        CMP transform1, -1
        JE  dato1
        JNE dato2

dato1:
      MOV transform1, Ah
      MOV aux, Al
      ADD transform1, 30h
      JMP dividir
dato2:
      MOV transform2, Ah
      MOV aux, Al      
      ADD transform2, 30h
      MOV Dx,0
      MOV Ah, 02H
      MOV DL, transform2
      INT 21h
      MOV DL,0
      MOV Ah, 02H
      MOV DL, transform1
      INT 21h
      MOV transform1, -1
      MOV transform2, 0
      JMP auxCursor
finalizar:
          JMP cleaneimp   
imprimirFinal:
       MOV DX, offset endstr
       mov Ax, 0
       mov Ah, 09h
       INT 21h          
terminar:
         MOV Ax,0
         MOV Ah,00h
         INT 21h 
ret
